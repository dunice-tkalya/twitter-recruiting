function drawCircle (canvas, cx, cy, r, stroke, strokeWidth, fill, grafClass) {
  canvas.append('circle')
    .attr('cx', cx)
    .attr('cy', cy)
    .attr('r', r)
    .attr('stroke', stroke)
    .attr('stroke-width', strokeWidth)
    .attr('fill', fill)
    .attr('class', grafClass)
}

function drawDottedLine (canvas, stroke, strokeWidth, x1, y1, x2, y2, grafClass) {
  canvas.append('line')
    .attr('stroke', stroke)
    .attr('stroke-width', strokeWidth)
    .style("stroke-dasharray", ("2, 2"))
    .attr('x1', x1)
    .attr('y1', y1)
    .attr('x2', x2)
    .attr('y2', y2)
    .attr('class', grafClass);
}


function drawLine (canvas, stroke, strokeWidth, x1, y1, x2, y2, grafClass) {
  canvas.append('line')
    .attr('stroke', stroke)
    .attr('stroke-width', strokeWidth)
    .attr('x1', x1)
    .attr('y1', y1)
    .attr('x2', x2)
    .attr('y2', y2)
    .attr('class', grafClass);
}

function drawPath (canvas, xTranslate, yTranslate, d, fill, stroke, strokeWidth, grafClass) {
  canvas.append('path')
    .attr('transform', 'translate(' + xTranslate + ',' + yTranslate + ')')
    .attr('d', d)
    .attr('fill', fill)
    .attr('stroke', stroke)
    .attr('stroke-width', strokeWidth)
    .attr('class', grafClass);
}

function drawPolyline (canvas, xTranslate, yTranslate, points, fill, stroke, strokeWidth, grafClass) {
  canvas.append('polyline')
    .attr('transform', 'translate(' + xTranslate + ',' + yTranslate + ')')
    .attr('points', points)
    .attr('fill', fill)
    .attr('stroke', stroke)
    .attr('stroke-width', strokeWidth)
    .attr('class', grafClass);
}

function drawPolygon (canvas, xTranslate, yTranslate, points, fill, stroke, strokeWidth, grafClass) {
  canvas.append('polygon')
    .attr('transform', 'translate(' + xTranslate + ',' + yTranslate + ')')
    .attr('points', points)
    .attr('fill', fill)
    .attr('stroke', stroke)
    .attr('stroke-width', strokeWidth)
    .attr('class', grafClass);
}

function drawRect (canvas, width, height, x, y, fill, grafClass, opacity) {
  canvas.append('rect')
    .attr('width', width)
    .attr('height', height)
    .attr('x', x)
    .attr('y', y)
    .attr('fill', fill)
    .attr('class', grafClass)
    .style('opacity', opacity)
}

function drawRectButton (canvas, width, height, x, y, fill, grafClass, rx, ry, stroke, strokeWidth) {
  canvas.append('rect')
    .attr('width', width)
    .attr('height', height)
    .attr('x', x)
    .attr('y', y)
    .attr('fill', fill)
    .attr('class', grafClass)
    .attr('rx', rx)
    .attr('ry', ry)
    .attr('stroke', stroke)
    .attr('stroke-width', strokeWidth);
}

function drawText (canvas, x, y, fill, size, anchor, text, grafClass, fontFamily) {
  canvas.append('text')
    .attr('x', x)
    .attr('y', y)
    .attr('fill', fill)
    .attr('font-size', size)
    .attr('text-anchor', anchor)
    .text(text)
    .attr('class', grafClass)
    .attr('font-family', fontFamily);
}

function drawImage (canvas, x, y, width, height, link, opacity, grafClass) {
    canvas.append('g').append('image')
        .attr('xlink:href', link)
        .attr('width', width)
        .attr('height', height)
        .attr('x', x)
        .attr('y', y)
        .style('opacity', opacity)
        .attr('class', grafClass)
}

function addTspan (canvas, dx, text, grafClass) {
    canvas.append('tspan')
      .attr('dx',dx)
      .text(text)
      .attr('class', grafClass)
}

